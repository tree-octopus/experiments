#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
uniform vec3 color;
void main(){
	vec2 st = gl_FragCoord.xy/u_resolution;
    float pct = 0.0;
    	
    pct = 1.0 - pow(distance(st,vec2(0.5)), 3.4360);
    vec3 old_color = vec3(0.99);
    vec3 color_mod = color * vec3(pct);

	//gl_FragColor = vec4( vec3(pct), pct*abs(sin(u_time)));
	gl_FragColor = vec4(color_mod, mix(1. - pct,  0.23*abs(sin(u_time/3.)), 0.35));
	//gl_FragColor = vec4(vec3(pct), 1.-1.1*pct);
	
}
