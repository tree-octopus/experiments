# MetroMap
*Processing, Shaders, Data-Enhanced*

<img src="https://i.imgur.com/FOVrSGt.gif" alt="" width="540">

Generative Metro system Map. 

Uses random building names from https://en.wikipedia.org/wiki/List_of_building_types, and random city names from https://www.samcodes.co.uk/project/markov-namegen/,
and sends the colors to http://www.thecolorapi.com/ to find the closest named colors for the Metrolines.

The background is generated using Voronoi diagram shaders based on the Manhattan and the L^infty Distance metrics