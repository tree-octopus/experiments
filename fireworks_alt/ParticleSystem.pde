import oscP5.*;
import netP5.*;

class ParticleSystem
{
  // position
  float xpos, ypos;
  // dimensions
  int w, h;
  // radius
  int radius = 150;
  // colors to interpolate between
  color c1, c2;
  //used for calculating noise
  float noise_val;
  float xoff = 0, yoff = 0;
  float noise_scale = 0.02;
  // used for fading out
  float age = 0;
  float max_age = 6;

  // how much of original velocity is kept after a collision
  float restitution = 1;

  // global check flags
  boolean checkBounds = false, checkColls = false;

  // color scheme
  //color[] colors = {#AF0F55, #4DA9EA};  
  color[] colors = {#09814A, #820263}; 
  color curr_color;
  PShader shader = loadShader( "distance_fade_shader.frag");

  ParticleSystem()
  {
  }

  // make a new particle system 
  // - check boundaries, collisions or not
  ParticleSystem(boolean bound, boolean coll)
  {
    checkBounds = bound;
    checkColls = coll;
    colorShift();
  }

  void colorShift()
  {
    colorMode(HSB, 360, 100, 100);
    float hue_shift = random(30, 331);
    colors[0] = color((hue(colors[0]) + hue_shift)%360, saturation(int(random(50, 100))), brightness(int(random(50, 100))));
    colors[1] = color((hue(colors[1]) + hue_shift)%360, saturation(int(random(50, 100))), brightness(int(random(50, 100))));
    println(hue(colors[0]));
    println(hue(colors[1]));
    colorMode(RGB, 255, 255, 255);
  }
  
  

  float start_time, lerp_time = 5000;
  color src1, src2, dest1, dest2;
  boolean is_threading = false;
  void colorShift_threaded()
  {
    if (!is_threading)
    {
      colorMode(HSB, 360, 100, 100);
      float hue_shift = random(30, 300);
      dest1 = color((hue(colors[0]) + hue_shift)%360, saturation(int(random(50, 100))), brightness(int(random(50, 100))));
      dest2 = color((hue(colors[1]) + random(0.5, 1.2)*hue_shift)%360, saturation(colors[1]), brightness(colors[1]));
      src1 = colors[0];
      src2 = colors[1];
      colorMode(RGB, 255, 255, 255);
      start_time = millis(); 
      thread("color_thread");
      is_threading = true;
    }
  }

  // current particle;
  Particle curr;

  public void update()
  {         
    blendMode(ADD);
    // update particles
    shader(shader);  
    shader.set("u_resolution", float(width), float(height));
    shader.set("u_time", millis() / 1000.0);
    //println("particles: "+particles_array.size());
    for (int i = 0; i < particles_array.size(); i++)
    {
      curr = particles_array.get(i);

      // remove dead particles
      if (curr.is_dead)
        particles_array.remove(i);
      else
      {
        // only make a new branch if there are still branches remaining, and 70% of that time
        if (particles_array.size() > 0 && random(10) >= 7 && particles_array.size() < 100)
        {

          seeder = particles_array.get((int)random(particles_array.size()));
          // additionally, only make a new branch if the branches are older than half their max age
          if (seeder.age/seeder.max_age >= 0.5)
          {
            explode((int)seeder.xpos, (int)seeder.ypos, (int)random(3), seeder.get_curr_radius());
          }
        }  
        shader(shader);  
        noiseDetail(3, 0.5);
        shader.set("phase", noise((curr.xpos+xoff*-1.2)*noise_scale, (curr.ypos+yoff*0.7)*noise_scale));
        //noiseDetail(4,0.65);
        //shader.set("freq", (float)2*PI*noise(curr.xpos*1., curr.ypos*1.));
        //println(curr.get_curr_radius());
        shader.set("rad_scale", (float)169.49 * pow(curr.get_curr_radius()/8., -0.953));
        shader.set("power", map(noise(curr.xpos, curr.ypos), 0, 1, 0.5, 1.6));
        shader.set("pos", (float)curr.xpos, (float)(height - curr.ypos));

        noise_val = noise((curr.xpos + xoff)*noise_scale, (curr.ypos + yoff) * noise_scale);

        curr_color = color(map(noise_val, 0, 1, red(colors[0]), red(colors[1])), 
          map(noise_val, 0, 1, green(colors[0]), green(colors[1])), 
          map(noise_val, 0, 1, blue(colors[0]), blue(colors[1])));       

        shader.set("base_color", red(curr_color)/255., green(curr_color)/255, blue(curr_color)/255);

        curr.update();
      }
    }
    xoff += 0.02;
    yoff += -0.03;
  }

  Particle seeder;  

  // how many particles to send out
  int num_particles;
  // where to store the particles
  ArrayList<Particle> particles_array = new ArrayList<Particle>();
  float init_rad = 1;
  // particles have velocity and other stuff
  boolean is_particle;
  // particle velocity stufssadf
  PVector velocity = new PVector(1, 1);
  float rand_walk = 4.;
  // to deal with particles aging and being removed
  boolean is_dead;
  // how fast particles age
  float age_factor = 0.04;
  float grow_start_rad;

  // spawn particles radially
  public void explode(int mouseX, int mouseY)
  {
    if ( particles_array.size() < 200)
    {
      xpos = mouseX;
      ypos = mouseY;
      num_particles = (int)random(5);  
      //num_particles = (int)random(3, 8);
      Particle temp;
      float vel_x, vel_y, vel_scale;
      float phase = random(1)*PI;
      for (int i = 0; i < num_particles; i++)
      {
        // make a new particle radially distributed around the original particle

        // randomly scale velocity
        vel_scale = random(0.2, 3) - random(1, 3);
        vel_x = vel_scale*cos(i*2*PI/num_particles+random(-PI/20, PI/20)+phase);
        vel_y = vel_scale*sin(i*2*PI/num_particles+random(-PI/20, PI/20)+phase);
        temp = new Particle(xpos+init_rad*vel_x, 
          ypos+init_rad*vel_y, (int)random(1, radius));
        temp.velocity.set(vel_x, vel_y);
        temp.max_age *= random(1, 2);
        temp.checkBounds = false;
        temp.rand_walk = this.rand_walk*.5;
        temp.restitution = this.restitution;
        temp.is_base = true;

        // osc stuff
        //temp.freq = i*2.*PI/num_particles+random(-PI/20,PI/20)+phase;
        temp.freq = i+1;
        //temp.freq = map(temp.freq, 0, 2*PI, 0, 12);
        temp.gain =  (float)0.2*(i+1)/num_particles;

        //println(temp.freq);
        //temp.sendBase(oscP5, myRemoteLocation);
        particles_array.add(temp);
      }
    }
  } 

  public void explode(int mouseX, int mouseY, int branch_num, float curr_rad)
  {
    if ( particles_array.size() < 200)
    {
      xpos = mouseX;
      ypos = mouseY;
      num_particles = (int)random(0, branch_num);  
      Particle temp;
      float vel_x, vel_y, vel_scale;
      float phase = random(1)*PI;
      for (int i = 0; i < num_particles; i++)
      {
        // make a new particle radially distributed around the original particle

        // randomly scale velocity
        vel_scale = random(0.2, 3) - random(1, 3);
        vel_x = vel_scale*cos(i*2*PI/num_particles+random(-PI/20, PI/20)+phase);
        vel_y = vel_scale*sin(i*2*PI/num_particles+random(-PI/20, PI/20)+phase);
        temp = new Particle(xpos+init_rad*vel_x, 
          ypos+init_rad*vel_y, (int)curr_rad);

        temp.velocity.set(vel_x, vel_y);
        temp.max_age /= random(3, 8);
        temp.checkBounds = false;
        temp.rand_walk = this.rand_walk*.8;
        temp.restitution = this.restitution;
        temp.is_harm = true;
        particles_array.add(temp);
      }
    }
  }

  // only make tress spawnable if there's no more branches to draw
  public boolean coolDown()
  {
    return particles_array.size() <= 5;
  }
}