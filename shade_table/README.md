# shade table
*ofx, GLSL*

<a href="https://youtu.be/v8F5gchI9og"><img src="https://imgur.com/ncv3QQ3.gif" alt="" width="40%" height="40%"></a>


Shader-based wave table synth written in openFrameworks.

Uses magnitude of color from a shader to generate a wave table.

[Video of the synth in action](https://youtu.be/v8F5gchI9og)
## Controls:
* *j*, *l* - increase/decrease frequency by 10 Hz
* *i*, *k* - increase/decrease amplitude by 0.1
* *w*, *s* - move the wave table cursor up/down
* *a*, *d* - increase/decrease the shader's time-scale
* *left*, *right* - change shaders

List of shaders:
....
