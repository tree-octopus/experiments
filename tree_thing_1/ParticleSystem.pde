class ParticleSystem
{
  // position
  float xpos, ypos;
  // dimensions
  int w, h;
  // radius
  int radius = 15;
  // colors to interpolate between
  color c1, c2;
  //used for calculating noise
  float noise_val;
  float xoff = 0, yoff = 0;
  float noise_scale = 0.02;
  // used for fading out
  float age = 0;
  float max_age = 6;

  // how much of original velocity is kept after a collision
  float restitution = 1;

  // global check flags
  boolean checkBounds = false, checkColls = false;

  // color scheme
  color[] colors = {#114B5F, #114B5F, #456990, #E4FDE1, #F45B69, #6B2737, #6B2737};    

  LeafSystem ls;

  ParticleSystem()
  {
  }

  // make a new particle system 
  // - check boundaries, collisions or not
  // - leaf system to refer to
  ParticleSystem(boolean bound, boolean coll, LeafSystem leafy)
  {
    checkBounds = bound;
    checkColls = coll;
    ls = leafy;
  }

  public void update()
  {         
    // update particles
    for (int i = 0; i < particles_array.size(); i++)
    {
      // remove dead particles
      if (particles_array.get(i).is_dead)
        particles_array.remove(i);
      else
      {
        // only make a new branch if there are still branches remaining, and 70% of that time
        if (particles_array.size() > 0 && random(10) >= 7)
        {

          seeder = particles_array.get((int)random(particles_array.size()));
          // additionally, only make a new branch if the branches are older than half their max age
          if (seeder.age/seeder.max_age >= 0.5)
          {
            explode((int)seeder.xpos, (int)seeder.ypos, (int)random(3), seeder.get_curr_radius());
          }
          // only add leaves if the branches are 70% of max age & are below certain size threshold
          if ((seeder.age/seeder.max_age >= 0.7 && seeder.get_curr_radius() <= 20) || seeder.get_curr_radius() <= 12)
            ls.explode((int)seeder.xpos, (int)seeder.ypos, (int)random(3), seeder.get_curr_radius()/radius);
        }        
        particles_array.get(i).update();
        //particles_array.get(i).velocity.y -=  0.03*(cos(map(noise(millis(), 1-millis()*2*PI), 0, 1, 0, 2*PI))+1);
        particles_array.get(i).velocity.y -=  0.03*(noise(millis(), 1-millis()*2*PI));
      }
    }
    // update leaves after particles
    ls.update();
  }

  Particle seeder;  

  // how many particles to send out
  int num_particles;
  // where to store the particles
  ArrayList<Particle> particles_array = new ArrayList<Particle>();
  float init_rad = 1;
  // particles have velocity and other stuff
  boolean is_particle;
  // particle velocity stufssadf
  PVector velocity = new PVector(1, 1);
  float rand_walk;
  // to deal with particles aging and being removed
  boolean is_dead;
  // how fast particles age
  float age_factor = 0.04;
  float grow_start_rad;

  // spawn particles radially
  public void explode(int mouseX, int mouseY)
  {
    xpos = mouseX;
    ypos = mouseY;
    num_particles = (int)random(5, 20);  
    Particle temp;
    float vel_x, vel_y, vel_scale;
    float phase = random(1)*PI;
    for (int i = 0; i < num_particles; i++)
    {
      // make a new particle radially distributed around the original particle

      // randomly scale velocity
      vel_scale = random(0.2, 3) - random(1, 3);
      vel_x = vel_scale*cos(i*2*PI/num_particles+random(-PI/20, PI/20)+phase);
      vel_y = vel_scale*sin(i*2*PI/num_particles+random(-PI/20, PI/20)+phase);
      temp = new Particle(xpos+init_rad*vel_x, 
        ypos+init_rad*vel_y, (int)random(1, radius));
      temp.velocity.set(vel_x, vel_y);
      temp.max_age *= random(1, 2);
      temp.checkBounds = false;
      temp.restitution = this.restitution;
      particles_array.add(temp);
    }
  } 

  public void explode(int mouseX, int mouseY, int branch_num, float curr_rad)
  {
    xpos = mouseX;
    ypos = mouseY;
    num_particles = (int)random(0, branch_num);  
    Particle temp;
    float vel_x, vel_y, vel_scale;
    float phase = random(1)*PI;
    for (int i = 0; i < num_particles; i++)
    {
      // make a new particle radially distributed around the original particle

      // randomly scale velocity
      vel_scale = random(0.2, 3) - random(1, 3);
      vel_x = vel_scale*cos(i*2*PI/num_particles+random(-PI/20, PI/20)+phase);
      vel_y = vel_scale*sin(i*2*PI/num_particles+random(-PI/20, PI/20)+phase);
      temp = new Particle(xpos+init_rad*vel_x, 
        ypos+init_rad*vel_y, (int)curr_rad);
      temp.velocity.set(vel_x, vel_y);
      temp.max_age /= random(3, 8);
      temp.checkBounds = false;
      temp.restitution = this.restitution;
      particles_array.add(temp);
    }
  }

  // only make tress spawnable if there's no more branches to draw
  public boolean coolDown()
  {
    return particles_array.size() <= 1;
  }
}