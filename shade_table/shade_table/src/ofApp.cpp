#include "ofApp.h"
#include "ThreeDWave.h"
//#include "my_class.h"

//--------------------------------------------------------------
void ofApp::setup() {

	// OpenGL stuff
	// ===============================================
#ifdef TARGET_OPENGLES
	shader.load("shadersES2/shader");
#else
	if (ofIsGLProgrammableRenderer()) {
		disp_shader.load("shadersGL3/shader"); \
		frag.load("shadersGL3/basic_vert.vert", "shadersGL3/manhattan_voronoi_YB_v1.frag");
		//frag.load("shadersGL3/basic_vert.vert", "shadersGl3/basic_quad.frag");
		//frag.load("shadersGL3/basic_vert.vert", "shadersGL3/3d_cell_noise.frag");
		//frag.load("shadersGL3/basic_vert.vert", "shadersGL3/pixelized_radius_thing.frag");
		//disp_shader.load("shadersGL3/basic_vert.vert", "shadersGL3/3d_cell_noise.frag");
	}
	else {
		disp_shader.load("shadersGL2/shader");
	}
#endif
	// ===============================================

	//audio stuff
	// ===============================================
	sampleRate = 44100;
	initialBufferSize = 512;

	audio_out.resize(initialBufferSize);
	for (int i = 0; i < initialBufferSize; i++)
	{
		audio_out[i] = 0.0;
	}

	ofSoundStreamSetup(1, 0, this, sampleRate, initialBufferSize, 4);
	// ===============================================

	// graphics stuff
	// ===============================================
	color.r = ofRandom(0, 255);
	color.g = ofRandom(0, 255);
	color.b = ofRandom(0, 255);
	cout << color << endl;
	img.allocate(80, 60, OF_IMAGE_GRAYSCALE);

	fbo.allocate(ofGetWidth(), ofGetHeight());

	wave_table.resize(fbo.getWidth());
	for (int i = 0; i < fbo.getWidth(); i++)
	{
		wave_table[i] = 0;
	}
	cout << "wave_table size: " << fbo.getWidth() << endl;
	plane.set(800, 600, rows, cols);
	plane.mapTexCoordsFromTexture(fbo.getTextureReference());

	meshy = plane.getMesh();

	// audio thread starts before setup, so we need this to avoid null pointers
	is_ready = true;
	// ===============================================
}

//--------------------------------------------------------------
void ofApp::update() {

	// calculate the wave grabbed based on current image
	// ===============================================
	int w = fbo.getWidth();
	int h = fbo.getHeight();

	ofPixels pixels;
	fbo.getTexture().readToPixels(pixels);
	ofImage image;

	image.setFromPixels(pixels);

	for (int x = 0; x < w; ++x)
	{
		ofColor c = image.getColor(x, wave_grabbed);
		wave_table[fmod(0 + x, wave_table.size())] = MIN((sqrtf(c.r * c.r + c.g * c.g + c.b * c.b) / 441.67294) * ampl, 1.0) * 2 - ampl;
	}
	// ===============================================
}

//--------------------------------------------------------------
void ofApp::draw() {
	
	// drawing the preview image of the wavetable
	// ===============================================
	fbo.begin();
	frag.begin();
	frag.setUniform1f("u_time", ofGetElapsedTimef()*time_scale);
	frag.setUniform2f("u_resolution", ofGetWidth() * 1., ofGetHeight() * 1.);
	frag.setUniform2f("u_mouse", mouseX * 1., mouseY * 1.);
	ofRect(0, 0, fbo.getWidth(), fbo.getHeight());
	frag.end();
	fbo.end();
	// preview
	ofSetColor(ofColor::white);
	fbo.draw(0, 0, fbo.getWidth() * preview_scale, fbo.getHeight() * preview_scale);
	ofSetColor(ofColor::crimson);
	ofDrawLine(0, wave_grabbed * preview_scale, fbo.getWidth() * preview_scale, wave_grabbed * preview_scale);

	// bind texture so the other shader can use it
	fbo.getTextureReference().bind();
	// ===============================================

	// displacement shader
	// ===============================================
	disp_shader.begin();
	disp_shader.setUniform1f("time", ofGetElapsedTimef());
	disp_shader.setUniform2f("u_resolution", ofGetWidth() * 1., ofGetHeight() * 1.);
	disp_shader.setUniform1f("scale", 200.);
	//cout << color << endl;
	//disp_shader.setUniform4f("base_color", color.r / 255., color.g / 255., color.b / 255., color.a / 255.);
	ofPushMatrix();

	// translate plane into center screen.
	float tx = ofGetWidth() / 2;
	float ty = ofGetHeight() / 2;
	ofTranslate(tx, ty);

	// the mouse/touch Y position changes the rotation of the plane.
	//float percentY = mouseY / (float)ofGetHeight();
	float percentY = 0.45;
	float rotation = ofMap(percentY, 0, 1, -60, 60, true) + 60;
	ofRotate(rotation, 1, 0, 0);

	// drawing the displaced plane	
	plane.draw();

	ofPopMatrix();

	disp_shader.end();

	ofSetColor(ofColor::white);
	// ===============================================


	// draw the base wave
	// ===============================================
	// draw the 0-line
	//ofSetColor(ofColor::black);
	//ofDrawLine(0, ofGetHeight() / 2,
	//	ofGetWidth(), ofGetHeight() / 2);

	ofSetColor(ofColor::teal);
	float amplitude = 30.0f;
	float offset = -8 * amplitude;
	for (int i = 1; i < wave_table.size(); i++) {

		// get two pairs of points
		float x1 = i * ofGetWidth() / (float)wave_table.size();
		float y1 = amplitude * wave_table[i] + offset;
		float x2 = (i - 1) * ofGetWidth() / (float)wave_table.size();
		float y2 = amplitude * wave_table[i - 1] + offset;

		// draw a tiny segment of the overall line
		ofDrawLine(x1, -y1 + ofGetHeight() / 2,
			x2, -y2 + ofGetHeight() / 2);
	}
	// ===============================================

	// draw the current wave at current frequency
	// ===============================================
	offset = -3 * amplitude;
	// draw the 0-line
	//ofSetColor(ofColor::black);
	//ofDrawLine(0, ofGetHeight() / 2 + offset,
	//	ofGetWidth(), ofGetHeight() / 2 + offset);
	ofSetColor(ofColor::red);
	for (int i = 1; i < audio_out.size(); i++) {

		// get two pairs of points
		float x1 = i * ofGetWidth() / (float)audio_out.size();
		float y1 = amplitude * audio_out[i] + offset;
		float x2 = (i - 1) * ofGetWidth() / (float)audio_out.size();
		float y2 = amplitude * audio_out[i - 1]  + offset;

		// draw a tiny segment of the overall line
		ofDrawLine(x1, -y1 + ofGetHeight() / 2,
			x2, -y2 + ofGetHeight() / 2);
	}
	// ===============================================

	// text stuff
	// ===============================================
	ofSetColor(ofColor::coral);
	ofDrawBitmapString("freq: " + ofToString(freq), 20, (1 + 1) * 20 + fbo.getHeight() * preview_scale);
	ofDrawBitmapString("ampl: " + ofToString(ampl), 20, (2 + 1) * 20 + fbo.getHeight() * preview_scale);
	ofDrawBitmapString("wave: " + ofToString(wave_grabbed), 20, (3 + 1) * 20 + fbo.getHeight() * preview_scale);
	ofDrawBitmapString("time: " + ofToString(time_scale), 20, (4 + 1) * 20 + fbo.getHeight() * preview_scale);
	// ===============================================
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	switch (key)
	{
		// frequency
		case 'j':
			freq -= 10;
			freq = MAX(freq, 10.);
			break;
		case 'l':
			freq += 10;
			break;
		// amplitude
		case 'i':
			ampl += 0.1;
			break;
		case 'k':
			ampl -= 0.1;
			ampl = MAX(ampl, 0.);
			break;

		// change wave from wave table
		case 'w':
			wave_grabbed -= 5;
			wave_grabbed = MAX(wave_grabbed, 0.);
			break;
		case 's':
			wave_grabbed += 5;
			wave_grabbed = MIN(wave_grabbed, fbo.getHeight() - 1.);
			break;
		// change time scale
		case 'a':
			time_scale -= time_scale_step;
			break;
		case 'd':
			time_scale += time_scale_step;
			break;

		// change shader
		case OF_KEY_LEFT:
			curr_wave -= 1;
			curr_wave += waves.size();
			curr_wave %= waves.size();
			frag.load("shadersGL3/basic_vert.vert", waves[curr_wave]);
			break;
		case OF_KEY_RIGHT:
			curr_wave += 1;
			curr_wave += waves.size();
			curr_wave %= waves.size();
			frag.load("shadersGL3/basic_vert.vert", waves[curr_wave]);
			break;


	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}

void ofApp::audioOut(float * output, int bufferSize, int nChannels) {
	if (initialBufferSize != bufferSize) {
		ofLog(OF_LOG_ERROR, "your buffer size was set to %i - but the stream needs a buffer size of %i", initialBufferSize, bufferSize);
		return;
	}
	else if (is_ready)
	{

		float wave_table_freq = sampleRate / wave_table.size();
		float old_ph = 0;
		//cout << "wave_table_freq " << wave_table_freq << endl;
		//cout << "HEY" << endl;
		for (int i = 0; i < bufferSize; i++)
		{
			//ph++;
			output[i] = ofLerp(wave_table[ph], wave_table[old_ph], 0.5);
			//output[i] = ofClamp(ofLerp(wave_table[(int)floor(old_ph) % wave_table.size()], wave_table[(int)floor(ph) % wave_table.size()], abs((old_ph)/ph)), 0., 1.);

			//output[i] = (output[i] + output[((i - 1) + bufferSize) % bufferSize]) * 0.5;

			audio_out[i] = output[i];
			//output[i] = wave_table[ph];
			////cout << "output: " << output[i] << endl;
			old_ph = ph;
			ph += freq / wave_table_freq;
			ph = fmod(ph, wave_table.size());
		}
	}
}

void ofApp::audioIn(float * input, int bufferSize, int nChannels) {

	memset(input, 0, nChannels * bufferSize * sizeof(float));

}
