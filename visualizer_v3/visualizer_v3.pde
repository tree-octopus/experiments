// import
// ============================================== //
import oscP5.*;
import netP5.*;
// ============================================== //

// OSC stuff
// ============================================== //
OscP5 oscP5;
NetAddress myRemoteLocation;
int in_port = 60000;
// ============================================== //

// graphics stuff
// ============================================== //
PShader star_shader;
PShader cloud;

float cooldown = 2200;
float start_time;

float seedA, seedB;
float screen_rad;
PGraphics star_buffer;
PGraphics galaxy_buffer;

// sky background
PShader sky_bg;
PGraphics sky_buffer;

// a flash!! of light
PGraphics flash_buffer;
boolean flashed = false;

PImage screen_shot;
color[] colors = {#354DFF, #3B31C5, #F7BC0C, #F7DF99};
// ============================================== //

void setup()
{
  // basic setup stuff
  size(800, 800, P2D);
  //fullScreen(P2D);
  screen_rad = sqrt(width * width + height * height);

  // osc receive stuff
  oscP5 = new OscP5(this, in_port);

  star_shader = loadShader("stars_v2.5.frag");
  star_buffer = createGraphics(width, height, P2D);

  cloud = loadShader("clouds_at_night.frag");
  galaxy_buffer = createGraphics(width, height, P2D);

  flash_buffer = createGraphics(width, height, P2D);
  flash_buffer.smooth(4);

  sky_bg = loadShader("simple_bg.frag");
  sky_buffer = createGraphics(width, height, P2D);

  background(240);

  // seeds for different randomizations of the cloud shaders
  seedA = random(1);
  seedB = random(1);

  frameRate(100);
}

void draw()
{
  blendMode(BLEND);
  //if (draw_trails)
  //{
  //  fill(10, 50);
  //  rect(0, 0, width, height);
  //}  


  // sky behind buffer
  // ============================================== //
  sky_buffer.beginDraw();
  sky_buffer.shader(sky_bg);
  sky_bg.set("u_resolution", float(width), float(height));
  sky_bg.set("u_mouse", float(mouseX), float(height - mouseY));
  sky_bg.set("u_time", millis() / 5000.0 + 400.);  
  sky_bg.set("seed", seedA);

  sky_buffer.rectMode(CORNER);
  sky_buffer.rect(0, 0, width, height);

  sky_buffer.endDraw();
  // ============================================== //

  // dark clouds
  // ============================================== // 
  if (invert)
  {

    cloud.set("u_time", millis() / 1000.0 + 400.);
    galaxy_buffer.beginDraw();
    galaxy_buffer.clear();

    galaxy_buffer.shader(cloud);    
    galaxy_buffer.rect(0, 0, width, height);    

    galaxy_buffer.endDraw();

    invert_alpha = map((millis() - invert_time)/invert_time_thresh, 0, 1, 100, 200);
  } else
  {
    cloud.set("u_time", invert_time / 1000.0 + 400. + centroid/1000.);
    invert_alpha = map((millis() - invert_time)/invert_time_thresh, 0, 1, 150, 15);
  }
  blendMode(SUBTRACT);   
  imageMode(CORNER);
  tint(240, invert_alpha);
  //println(invert_alpha);
  image(galaxy_buffer, 0, 0);
  // ============================================== // 

  blendMode(BLEND);

  // flash
  // ============================================== //
  flash_buffer.beginDraw();

  flash_buffer.resetShader();
  flash_buffer.blendMode(BLEND);

  if (flashed)
  {
    flashed = false;
    flash_buffer.blendMode(ADD);
    flash_buffer.fill(200, 20);

    flash_buffer.rect(0, 0, width, height);
  }

  flash_buffer.blendMode(BLEND);
  flash_buffer.imageMode(CORNER);
  flash_buffer.tint(200, map(centroid, 0, 100, 80, 200));
  flash_buffer.image(sky_buffer, 0, 0);

  flash_buffer.endDraw();

  imageMode(CORNER);
  resetShader();
  //blendMode(MULTIPLY);
  tint(255, 150);
  image(flash_buffer, 0, 0);
  // ============================================== //


  // cloud behind
  // ============================================== //
  blendMode(ADD);
  //resetShader();
  shader(cloud);
  cloud.set("u_resolution", float(width), float(height));
  cloud.set("u_mouse", float(mouseX), float(height - mouseY));
  cloud.set("u_time", millis() / 3000.0 + 400. + rms_thing);  
  cloud.set("seed", seedA);
  set_shader_color("color1", colors[0], cloud);
  set_shader_color("color2", colors[1], cloud);
  set_shader_color("color3", colors[2], cloud);
  set_shader_color("color4", colors[3], cloud);
  rectMode(CORNER);
  //fill(255, 0, 0);
  rect(0, 0, width, height);
  // ============================================== //


  // stars
  // ============================================== // 
  star_buffer.beginDraw();    
  star_buffer.clear();

  star_buffer.shader(star_shader);
  star_shader.set("u_resolution", float(width), float(height));
  star_shader.set("u_mouse", float(mouseX), float(height - mouseY));
  star_shader.set("u_time", millis() / 1000.0);

  star_buffer.rectMode(CENTER);
  star_buffer.ellipse(width/2, height/2, screen_rad, screen_rad);
  star_buffer.endDraw();

  // ------- //
  pushMatrix();
  translate(width/2, height/2);
  rotate(PI/50 * rms_thing*10.);

  blendMode(ADD);
  resetShader();
  imageMode(CENTER);  
  tint(255);
  image(star_buffer, 0, 0, screen_rad, screen_rad);  
  popMatrix();
  // ------- //  
  // ============================================== //


  // cloud front
  // ============================================== //
  blendMode(ADD);
  resetShader();
  shader(cloud);
  cloud.set("u_resolution", float(width), float(height));
  cloud.set("u_mouse", float(mouseX), float(height - mouseY));
  cloud.set("u_time", millis() / 3200.0 - 240. + rms_thing/2.);  
  cloud.set("seed", seedB+4.);
  set_shader_color("color1", colors[1] - colors[0], cloud);
  set_shader_color("color2", colors[0], cloud);
  set_shader_color("color3", colors[2], cloud);
  set_shader_color("color4", colors[3], cloud);
  rectMode(CORNER);
  rect(0, 0, width, height);
  //resetShader();
  
    //blendMode(BLEND);
  //resetShader();
  //blendMode(EXCLUSION);
  for(int i = 0; i < 5; i++)
  {
      shader(cloud);
      cloud.set("u_resolution", float(width), float(height));
      cloud.set("u_mouse", float(mouseX), float(height - mouseY));
      cloud.set("u_time", millis() / 3200.0 - 240. + rms_thing/2.);  
      cloud.set("seed", seedB+(i+1)*2.);
      set_shader_color("color1", colors[1] - colors[0], cloud);
      set_shader_color("color2", colors[0], cloud);
      set_shader_color("color3", colors[2], cloud);
      set_shader_color("color4", colors[3], cloud);
      rectMode(CORNER);
      rect(0, 0, width, height);
  }

  //resetShader();
  // ============================================== //
  
  
}

void set_shader_color(String c_name, color c, PShader ps)
{
  ps.set(c_name, (float)red(c)/255, (float)green(c)/255, (float)blue(c)/255);
}

// debug stuff
//void keyPressed()
//{
//  // check for reset key
//  if (key == CODED) {
//    // only reset if it's off of cooldown!!
//    if (keyCode == SHIFT && millis() >= start_time + cooldown)
//    {
//      flashed = true;
//      invert = !invert;
//    }
//  }
//}

float rms_thing = 0;
float centroid;

boolean invert = false;
float invert_time = 0, invert_time_thresh = 1200;
float invert_alpha;
// osc message receive!!
void oscEvent(OscMessage theOscMessage)
{
  //println("got something");
  //print(" addrpattern: "+theOscMessage.addrPattern());
  //println(" typetag: "+theOscMessage.typetag());
  if (theOscMessage.addrPattern().equals("/rms"))
  {
    float f = theOscMessage.get(0).floatValue();
    //println("rms: " + f);
    if (f >= 65)
    {
      //flashed = true;
      rms_thing += 0.1;
    }
    else if(f >= 70)
    {
       flashed = true; 
    }
  } else if (theOscMessage.addrPattern().equals("/rms2"))
  {

    flashed = true;
  } else if (theOscMessage.addrPattern().equals("/centroid"))
  {    
    float centroid = theOscMessage.get(0).floatValue();
    if (centroid >= 50 && millis() >= invert_time + invert_time_thresh)
    {
      invert = !invert;
      invert_time = millis();
      //println("!!" + invert_time);
    }
  }
}