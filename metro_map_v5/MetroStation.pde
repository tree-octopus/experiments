class MetroStation
{
  // list of lines this MetroStation is on
  ArrayList<MetroLine> lines = new ArrayList<MetroLine>();
  // the name of this MetroStation
  String name = "";
  PVector position = new PVector();

  // is it a terminal
  boolean is_end = false;
  // which line's end
  ArrayList<Integer> line_end = new ArrayList<Integer>();
  // is it a junction of multiple lines
  boolean is_junct = false;

  MetroSystem msys;


  MetroStation(String _name, PVector pos, MetroSystem _msys)
  {
    name = _name;
    position.set(pos);
    msys = _msys;
  }

  String num;
  void draw()
  {
    num = "";
    if (lines.size() > 1)
      is_junct = true;
    if (is_end && lines.size() == 1)
    {
      noStroke();
      textFont(msys.font);
      fill(red(lines.get(0).c), green(lines.get(0).c), blue(lines.get(0).c), 255);
      ellipse(position.x, position.y, msys.line_width*1.8, msys.line_width*1.8);
      rectMode(CENTER);
      fill(255);
      textSize(18);
    }    

    if (is_junct)
    {
      for (int i = 0; i < lines.size(); i++)
      {
        fill(red(lines.get(i).c), green(lines.get(i).c), blue(lines.get(i).c), 255);
        stroke(10);
        strokeWeight(1.2);
        noStroke();
        arc(position.x, position.y, 
          msys.line_width*1.6, msys.line_width*1.6, 
          2*PI*i/lines.size() + PI/4, 2*PI*(i+1)/lines.size() +PI/4, PIE);
      }
      noStroke();
      fill(20);
      ellipse(position.x, position.y, msys.line_width*.8, msys.line_width*.8);
      //noStroke();
      if (!is_end)
      {
        fill(255);
        ellipse(position.x, position.y, msys.line_width*1-1, msys.line_width*1-1);
      } else
      {

        fill(0);
      }
      fill(255);
      ellipse(position.x, position.y, msys.line_width*.6, msys.line_width*.6);
    } else if (!is_junct && !is_end)
    {
      //stroke(255);
      //strokeWeight(1);
      //fill(0);
      //ellipse(position.x, position.y, msys.line_width*0.8, msys.line_width*0.8);
      noStroke();
      fill(255);
      ellipse(position.x, position.y, msys.line_width*.6, msys.line_width*.6);
    }

    // draw terminal number last!!
    //if (is_end)
    //{
    //  fill(0);
    //  for (int i = 0; i < lines.size(); i++)
    //  {
    //    num += (lines.get(i).line_number+1) + " ";
    //  }
    //  text(num, 
    //    position.x- 0.85*textWidth(num)/2, 
    //    position.y + textAscent()/2 );
    //}
  }
}