class Particle
{
  // position
  float xpos, ypos;
  // dimensions
  int w, h;
  // radius
  int radius;
  // colors to interpolate between
  color c1, c2;

  //color[] colors = {#81D2C7, #F5EFED, #2292A4, #BDBF09, #d96c06, #d96c06}; 
  // pastel color scheme
  color[] colors = {#114B5F, #114B5F, #456990, #E4FDE1, #F45B69, #6B2737, #6B2737};
  // tree stuff
  //color[] colors = {#F3ECEE, #352C1C, #020B07};
  float tween;

  //used for calculating noise
  float noise_val;
  float xoff = 0, yoff = 0;
  float noise_scale = 0.02;
  // used for fading out
  float age = 0;
  float max_age = 6;

  // check boundaries or collisions
  boolean checkBounds = true, checkColls = false;

  // how much % of velocity to keep when colliding with something
  float restitution = 1;
  
  PVector vel_temp = new PVector();

  public Particle(float new_x, float new_y, int rad)
  {
    // set position & dimensions
    xpos = new_x;
    ypos = new_y;
    radius = rad;
    // set colors

    //c1 = colors[0];
    //c2 = colors[1];
  }

  public Particle(float new_x, float new_y, int rad, boolean bound, boolean coll)
  {
    // set position & dimensions
    xpos = new_x;
    ypos = new_y;
    radius = rad;

    checkBounds = bound;
    checkColls = coll;

    // set colors

    //c1 = colors[0];
    //c2 = colors[1];
  }

  // if we want to move it without changing dimensions 
  public void setPos(int x, int y)
  {   
    xpos = x;
    ypos = y;
  }    

  // draw the circle
  public void update()
  {        
    vel_temp.x = (velocity.x+noise_val*random(-rand_walk, rand_walk))*(1-0.4*age/max_age);
    vel_temp.y = (velocity.y+noise_val*random(-rand_walk, rand_walk))*(1-0.4*age/max_age);
    xpos += vel_temp.x;
    ypos += vel_temp.y;
    //noise_val = noise((xpos+xoff)*noise_scale, (ypos+yoff)*noise_scale);
    //xpos += .2*cos(map(noise_val, 0, 1, 0, 2*PI));
    //ypos += .2*sin(map(noise_val, 0, 1, 0, 2*PI));
    if (checkBounds)
      checkBounds();
    age += age_factor;
    // generate noise based on offset and position
    noiseDetail(7, 0.4);
    noise_val = noise((xpos+xoff)*noise_scale, (ypos+yoff)*noise_scale);
    //println("noise: "+noise_val);
    colormap(noise_val, 0, 1);
    // use noise to determine fill color
    //noStroke();
    stroke(0, (1-age/max_age)*10+5);
    fill(map(tween, 0, 1, red(c1), red(c2)), map(tween, 0, 1, green(c1), green(c2)), 
      map(tween, 0, 1, blue(c1), blue(c2)), (1-age/max_age)*255);

    size_mult = (1-age/max_age) * 2;
    rectMode(CENTER);
    // draw ellipse
    ellipse(xpos, ypos, radius*size_mult, radius*size_mult);

    
    // increment offset
    xoff+= 1.3;
    yoff-= 1.1;

    is_dead = checkDeath();
  }

  private void checkBounds()
  {
    if (xpos+radius >= width || xpos-radius <= 0)
    {
      velocity.x = -restitution * velocity.x;
    }
    if (ypos+radius >= height || ypos-radius <= 0)
    {
      velocity.y = -restitution * velocity.y;
    }
  }

  /* Growth Stuff */
  float size_mult;
  /* Growth Stuff */


  public float get_curr_radius()
  {
    return radius * size_mult;
  }

  PVector velocity = new PVector(1, 1);
  float rand_walk = 5;
  // to deal with particles aging and being removed
  boolean is_dead;
  // how fast particles age
  float age_factor = 0.04;

  // check if particle should be removed 
  public boolean checkDeath()
  {
    if (!checkBounds)
      return  ((xpos-radius >= width || xpos+radius <= 0 || 
        ypos-radius >= height || ypos+radius <= 0)) || 
        age > max_age;
    else  
    return age > max_age;
  }

  // for color stuff
  // - let noise value interpolate between the whole array of colors
  private void colormap(float orig_val, float orig_min, float orig_max)
  {
    float mark = (orig_val - orig_min) / (orig_max - orig_min);
    float perc = mark * (colors.length-1);

    tween = perc - floor(perc);
    //println(tween);
    c1 = colors[constrain(floor(perc), 0, colors.length-1)];
    c2 = colors[constrain(ceil(perc), 0, colors.length-1)];
  }
}