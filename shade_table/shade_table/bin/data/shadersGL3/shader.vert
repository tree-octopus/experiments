#version 150

// these are from the programmable pipeline system
uniform mat4 modelViewProjectionMatrix;
in vec4 position;
in vec2 texcoord;

// this is how we receive the texture
uniform sampler2DRect tex0;

out vec2 texCoordVarying;

uniform vec2 u_resolution;
out vec4 v_color;
uniform float time;
uniform float scale;

in vec4 normal;

void main()
{
    // get the position of the vertex relative to the modelViewProjectionMatrix
    vec4 modifiedPosition = modelViewProjectionMatrix * position;
    
    // we need to scale up the values we get from the texture
    
    // here we get the red channel value from the texture
    // to use it as vertical displacement
    float displacementY = texture(tex0, texcoord).r;

    // vec3 displacement = texture(tex0, texcoord).rgb;

    // float offset = displacementY * scale;
    // float tot = modifiedPosition.y + scale;


    // use the displacement we created from the texture data
    // to modify the vertex position
	modifiedPosition.y += displacementY * scale;
    // modifiedPosition.xyz += displacement * scale;
	
    // this is the resulting vertex position
    gl_Position = modifiedPosition;

    //v_color = vec4(displacementY,1. - (sin(time * 10.0)+1.) *0.5 * displacementY, 0., 0.8);

    // v_color = vec4(displacement, 0.8);
    v_color = vec4(displacementY, 1.0, modifiedPosition.z/u_resolution.y, 0.8);


    // pass the texture coordinates to the fragment shader
    texCoordVarying = texcoord;
}