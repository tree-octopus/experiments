# Fireworks alt
*Processing, Shaders*

<img src="https://i.imgur.com/DF7chPy.gif" alt="" width="300" height="300">

Fireworks-like partices that are spawned in a Lissajous-like curve.

Fireworks 'stars' are a Manhattan distance field. 
The Lissajous-like curve is modulated with noise, and a spring-mass-damper system from a Processing example.

One day I'll add rotation to the 'stars'...

