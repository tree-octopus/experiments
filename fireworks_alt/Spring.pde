// Adapted from the Processing example 
class Spring
{
  float mass, k, damp;
  PVector rest, pos, old_pos, vel, acc, force;

  float angle, dist, d_angle;

  Spring(float _mass, float _k, float _damp, PVector _rest, PVector _pos)
  {
    mass = _mass;
    k = _k;
    damp = _damp;
    rest = _rest;

    pos = _pos;
    old_pos = new PVector();
    vel = new PVector();
    acc = new PVector();
    force = new PVector();
  }

  void update()
  {
    force.x = -k * (pos.x - rest.x);
    force.y = -k * (pos.y - rest.y);

    acc.x = force.x/mass;
    acc.y = force.y/mass;

    vel.x = damp * (vel.x + acc.x);
    vel.y = damp * (vel.y + acc.y);

    pos.add(vel);

    if (abs(vel.x) < 0.1)
      vel.x = 0;
    if (abs(vel.y) < 0.1)
      vel.y = 0;

    old_pos.set(pos);

    //fill(255 * noise(pos.x + second(), pos.y + second()),255* noise(pos.x + 1.1 + 0.5*second(), pos.y + 1.1 + 0.4*second()), 0);
    noStroke();
    //blendMode(ADD);
    //ellipse(pos.x, pos.y, 50., 50.);
  }
}