# Experiments
Trying out things to make stuff look / sound good.

## Projects

### MetroMap
*Processing, Shaders*

<img src="https://i.imgur.com/FOVrSGt.gif" alt="" width="40%" height="40%">

Generative Metro system Map. 

### Visualizer v3
*Processing, Shaders, ChucK*

<img src="https://i.imgur.com/miXZ6ZN.gif" alt="" width="40%" height="40%">

Uses shaders to make a night sky music visualizer.

### Fireworks alt
*Processing, Shaders*

<img src="https://i.imgur.com/DF7chPy.gif" alt="" width="300" height="300">

Fireworks-like partices that are spawned in a Lissajous-like curve

### tree spawner
*Processing*

<img src="https://i.imgur.com/nFKnluw.gif" alt="" width="540" height="300">

Makes trees using a particle system