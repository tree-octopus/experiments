class Particle
{
  // position
  float xpos, ypos;
  // dimensions
  int w, h;
  // radius
  int radius;
  // colors to interpolate between
  color c1, c2;

  color[] colors = {#81F499, #81F499};
  color curr_color;
  float tween;

  //used for calculating noise
  float noise_val;
  float xoff = 0, yoff = 0;
  float noise_scale = 0.02;
  // used for fading out
  float age = 0;
  float max_age = 6;

  // check boundaries or collisions
  boolean checkBounds = true, checkColls = false;

  // for sound
  boolean is_base = false, is_harm = false;
  Particle base;
  float freq, gain;

  // how much % of velocity to keep when colliding with something
  float restitution = 1;

  float angle;

  PVector vel_temp = new PVector();

  int draw_meth;

  public Particle(float new_x, float new_y, int rad)
  {
    // set position & dimensions
    xpos = new_x;
    ypos = new_y;
    radius = rad;
    // set colors
    size_mult = (1-age/max_age) * 2;
    //c1 = colors[0];
    //c2 = colors[1];
    draw_meth = floor(random(1, 6));
  }

  public Particle(float new_x, float new_y, int rad, boolean bound, boolean coll)
  {
    // set position & dimensions
    xpos = new_x;
    ypos = new_y;
    radius = rad;
    size_mult = (1-age/max_age) * 2;
    checkBounds = bound;
    checkColls = coll;

    // set colors

    //c1 = colors[0];
    //c2 = colors[1];
    draw_meth = floor(random(1, 6));
  }

  // if we want to move it without changing dimensions 
  public void setPos(int x, int y)
  {   
    xpos = x;
    ypos = y;
  }    


  // draw the circle
  public void update()
  {        
    noise_val = noise((xpos+xoff)*noise_scale, (ypos+yoff)*noise_scale);

    // different methods of moving particles!!    
    switch(draw_meth)
    {
    case 1:
      vel_temp.x = (velocity.x+noise_val*random(-rand_walk, rand_walk))*(1-0.4*age/max_age);
      vel_temp.y = (velocity.y+noise_val*random(-rand_walk, rand_walk))*(1-0.4*age/max_age);
      break;
    case 2:
      vel_temp.x = 2*velocity.x + cos(noise_val*2*PI);
      vel_temp.y = 2*velocity.y + sin(noise_val*2*PI);
      break;
    case 3:
      vel_temp.x = 2*velocity.x * cos(noise_val*2*PI);
      vel_temp.y = 2*velocity.y * sin(noise_val*2*PI);
      break;
    case 4:
      vel_temp.x = 2 * cos(noise_val*2*PI) * sin(noise_val*2*PI);
      vel_temp.y = 2* sin(noise_val*2*PI) * cos(noise_val*2*PI);
      break;
    case 5:
      vel_temp.x = 1.5 * velocity.x * cos(map(noise_val, 0, 1, 0, 2*PI)) + random(-rand_walk, rand_walk);
      vel_temp.y = 1.5 * velocity.y * sin(map(noise_val, 0, 1, 0, 2*PI)) + random(-rand_walk, rand_walk);
      break;
    }

    // particles should be accelerating upwards a little
    vel_temp.y -= 1;    
    
    // update positions by velocity
    xpos += vel_temp.x;
    ypos += vel_temp.y; 

    if (checkBounds)
      checkBounds();
      
    age += age_factor;
    // generate noise based on offset and position
    
    //noiseDetail(7, 0.4);
    //noise_val = noise((xpos+xoff)*noise_scale, (ypos+yoff)*noise_scale);
    ////println("noise: "+noise_val);
    //colormap(noise_val, 0, 1);
    //// use noise to determine fill color
    //noStroke();


    //////stroke(0, (1-age/max_age)*10+5);
    //curr_color = color(map(tween, 0, 1, red(c1), red(c2)), map(tween, 0, 1, green(c1), green(c2)), 
    //  map(tween, 0, 1, blue(c1), blue(c2)), (1-age/max_age));
    //fill(curr_color);

    size_mult = (1-age/max_age) * 2 + 0.25;
    ellipseMode(CENTER);
        
    // draw ellipse  
    ellipse(xpos, ypos, radius*size_mult, radius*size_mult);


    // increment offset
    xoff+= 1.3;
    yoff-= 1.1;

    is_dead = checkDeath();
  }

  private void checkBounds()
  {
    if (xpos+radius >= width || xpos-radius <= 0)
    {
      velocity.x = -restitution * velocity.x;
    }
    if (ypos+radius >= height || ypos-radius <= 0)
    {
      velocity.y = -restitution * velocity.y;
    }
  }

  /* Growth Stuff */
  float size_mult;
  /* Growth Stuff */


  public float get_curr_radius()
  {
    return radius * size_mult;
  }

  PVector velocity = new PVector(1, 1);
  float rand_walk = 5;
  // to deal with particles aging and being removed
  boolean is_dead;
  // how fast particles age
  float age_factor = 0.04;

  // check if particle should be removed 
  public boolean checkDeath()
  {
    if (!checkBounds)
      return  ((xpos-radius >= width || xpos+radius <= 0 || 
        ypos-radius >= height || ypos+radius <= 0)) || 
        age > 0.9 * max_age || get_curr_radius() < 1;
    else  
    return age > 0.9 * max_age;
  }

  public float getPropAge()
  {
    return age/max_age;
  }

}