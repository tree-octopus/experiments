#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

#define PI 3.14159265359

varying float the_angle;
uniform float rot_angle;

uniform vec2 pos;
uniform vec3 color;
uniform float phase;
uniform float power;
uniform float freq;
uniform float rad_scale;
uniform vec3 base_color;

uniform float thresh = 0.001;

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}


float manhattan(vec2 a, vec2 b)
{
    return abs(a.x - b.x) + abs(a.y - b.y);    
}


float random (in vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(-0.120,0.660)))
                 * 43759.201);
}

void main(){
	vec2 st = gl_FragCoord.xy/u_resolution.xy;
    st -= vec2(0.5);
    st = rotate2d( rot_angle ) * st;
    st += vec2(0.5);


    float pct = 0.0;
    
    vec2 temp_st = st;
    temp_st *= rad_scale*1.;
    vec2 norm_pos = (pos/u_resolution);
    // a. The DISTANCE from the pixel to the center    
    //pct = 1.-pow(distance(st,pos/u_resolution), power);
    //pct = 1.- 0.5 * pow(distance(rad_scale*st,rad_scale*norm_pos), power);
    pct = 1.-pow(manhattan(rad_scale*st,rad_scale*norm_pos), power);
    
    vec3 col = vec3(pow(pct, 5.));
	gl_FragColor = vec4(col*base_color, (0.3)*pow(pct, 7.)+0.1*(0.8*fract(cos(u_time+fract(random(vec2(phase))))+1)));
    //gl_FragColor = vec4( col, clamp(0.5*pct-0.3, 0, 1));

    if(gl_FragColor.x <= thresh || gl_FragColor.y <= thresh ||  gl_FragColor.z <= thresh)
        gl_FragColor = vec4(0.);
}
