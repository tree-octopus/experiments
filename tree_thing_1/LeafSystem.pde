class LeafSystem
{
  // stores the same shape to be used across every leaf
  PShape shape;

  ArrayList<Leaf> leaves = new ArrayList<Leaf>();

  // global minimum & maximum z's
  float min_z = 0.001, max_z = 1000;

  // color of the leaves
  // red, white
  color[] col = {#CC264D, #F3ECEE};
  // green stuff
  //color[] col = {#98DFAF, #A0A0A0};
  //color col = #d96c06;
  // how much to make the leaves move randomly
  float rand_walk = 3.8;

  // start to make the shape
  LeafSystem()
  {   
    initShape();
  }

  // draws a leaf based on parabolas
  void initShape()
  {

    shape = createShape();
    shape.beginShape();
    //s.fill(0, 0, 255);
    //s.noStroke();

    for (int i = -3; i < 4; i++)
    {
      shape.vertex(i, sqrGraph(i, 0.5, 0, 1, -9));
    }
    for (int i = 5; i > -6; i--)
    {
      shape.vertex(i, sqrGraph(i, 1, 0, -0.8, -40));
    }
    shape.endShape(CLOSE);      
    //scale(5);
  }

  // make a shape to return
  // - because shapes are passed by reference, we don't want the master shape to be changed every time
  // - the leaf is drawn using parabolas
  PShape makeShape(PShape billys_shape, int x, int y)
  {       
    billys_shape = createShape();
    billys_shape.beginShape();
    float n = getNoiseVal(x+random(40), y+random(40), 0.02);
    // every leaf is the same color, but different shade
    billys_shape.fill(map(n, 0, 1, red(col[0]), red(col[1])), map(n, 0, 1, green(col[0]), green(col[1])), 
      map(n, 0, 1, blue(col[0]), blue(col[1])), 250);
    //billys_shape.stroke(0,0,0,5);

    for (int i = -3; i < 4; i++)
    {
      billys_shape.vertex(i, sqrGraph(i, 0.5, 0, 1, -9));
    }
    for (int i = 5; i > -6; i--)
    {
      billys_shape.vertex(i, sqrGraph(i, 1, 0, -0.8, -40));
    }
    billys_shape.endShape(CLOSE);      
    //scale(5);
    return billys_shape;
  }

  // use noise to modulate a color
  float getNoiseVal(float x, float y, float noise_scale)
  {
    return noise((x)*noise_scale, (y)*noise_scale);
  }

  void update()
  {      
    for (int i = 0; i < leaves.size(); i++)
    {
      // update every leaf
      leaves.get(i).update();
      // remove dead leaves
      if (leaves.get(i).isDead())
        leaves.remove(i);
    }
  }

  float xpos, ypos;
  // number of leaves to spawn with each 'set' of leaves
  int num_leaves_spawn;

  public void explode(int mouseX, int mouseY, int branch_num, float scale)
  {
    // spawn the leaves centered around this position 
    xpos = mouseX;
    ypos = mouseY;
    num_leaves_spawn = (int)random(0, branch_num);  
    Leaf temp;
    float phase = random(1)*PI;
    for (int i = 0; i < num_leaves_spawn; i++)
    {
      // make new leaves radially distributed around the original particle        
      temp = new Leaf(makeShape(new PShape(), (int)xpos, (int)ypos), xpos, 
        ypos, max_z/random(1, 3), PI/80*random(0, 4), PI/80*random(0, 4), i*2*PI/num_leaves_spawn+random(-PI/20, PI/20)+phase, min_z, max_z);

      // leaves don't fall at first
      temp.is_falling = false;
      temp.rand_walk = rand_walk * 1.5;
      // set the tumble rate of the leaves
      temp.d_rot = new PVector(PI/360*random(0, 4), PI/360*random(0, 4), PI/360*random(0, 4));
      temp.scale = min(4*scale + 0.5, 1);
      // add the leaf to current leaves
      leaves.add(temp);
    }
  }

  // for calculating leaf shape, uses a y = sqrt(x) graph
  float sqrtGraph(float x, float x_coef, float x_off, float y_coef, float y_off)
  {
    return y_coef * sqrt(x_coef * x - x_off) + y_off;
  }

  // for calculating leaf shape, uses a y = x^2 graph
  float sqrGraph(float x, float x_coef, float x_off, float y_coef, float y_off)
  {
    return  (y_coef *(x_coef * x - x_off) * (x_coef * x - x_off) - y_off);
  }

  // set all current leaves to fall
  void leafFall()
  {
    for (int i = 0; i < leaves.size(); i++)
    {
      leaves.get(i).is_falling = true;
    }
  }
}