class Leaf
{
  // shape of the Leaf
  PShape shape;  
  // position of the Leaf
  PVector position;
  // starting rotation
  PVector rotation = new PVector(1, 1, 1);
  // how rotation changes
  PVector d_rot = new PVector(0, 0, 0);
  
  

  // falling stuff
  float z_rate = 2;
  // z-coordinates for fading out
  // - max is how close
  // - min is how far
  float max_z = 1000, min_z = 0.01;
  // use the changing z to change scale of Leafs
  float z_scale;
  boolean is_falling = false;
  
  // how much z-scale is multiplied by
  float scale = 1;
  
  // how much random-ness to add to Leaf's position (taken from leaf system)
  float rand_walk = 1.5;
  
  boolean drawOnce = false;
  
  Leaf(PShape that_shape, float x, float y, float z)
  {
    shape = that_shape;
    position = new PVector(x, y, z);
    updateScale();
  }
  
  /** create a leaf based on a 
   * - shape
   * - x, y, z starting coordinates
   * - inital rotation
   * - minimum z
   * - maximum z
  **/
  Leaf(PShape that_shape, float x, float y, float z, float x_rot, float y_rot, float z_rot, float tmin_z, float tmax_z)
  {
    shape = that_shape;
    position = new PVector(x, y, z);
    rotation = new PVector(x_rot, y_rot, z_rot);
    shape.rotateZ(rotation.z);
    shape.rotateY(rotation.y);
    shape.rotateX(rotation.x);
    
    min_z = tmin_z;
    max_z = tmax_z;
    
    updateScale();
  }
  
  // calculate new scale based on updated z-position
  void updateScale()
  {
    z_scale = scale * position.z/max_z;
  }
  
  void update()
  {
    // only draw leaf if it's not dead
    if(!isDead())
    {      
      updateScale();
      // make sure transformation matrix isn't applied to the rest of the sketch
      // - so each Leaf has its own transformation matrix because scale and roation change with each Leaf
      pushMatrix();
      scale(z_scale);
      //translate(width/2 * 1/z_scale, height/2 * 1/z_scale);           
      //shape.setFill(0, 0, 255);
      // rotate leaf and decrease its z-position while it's falling
      if(is_falling)
      {
          position.z -= z_rate *random(1,1.5);
          shape.rotateZ(d_rot.z);
          shape.rotateY(d_rot.y);
          shape.rotateX(d_rot.x);
          
          // only do random walk every 3 frames so it's smoother
          if(frameCount % 3 == 0)
          {
              position.x += (random(-rand_walk,rand_walk));
              position.y += (random(-rand_walk,rand_walk));  
          }     
      }
      // only draw Leafs once before they fall
      if(!is_falling && !drawOnce)
      {
          shape(shape, position.x/z_scale, position.y/z_scale);
          drawOnce = true;
      }
      // if Leaf is falling, draw it every frame
      else if(is_falling)
      {
          shape(shape, position.x/z_scale, position.y/z_scale);
      }
      
      // remove last transformation matrix
      popMatrix();
    }
  }
  
  // check if the Leaf is past the minimum size, and set it for removal
  boolean isDead()
  {
     return position.z <= min_z; 
  }
  
}