#pragma once
class ThreeDWave
{
public:
	ThreeDWave(float x_off, float y_off);
	virtual ~ThreeDWave();

	float getVal(float x, float y);
	bool isDead();
	void excite();

	float noise_vel_scale = 100;
	float noise_vel;

	float x_off, y_off;

	float exp_scale = 0.0008;
	float start_time = 0.0;

	float thresh = 0.0001;
};

