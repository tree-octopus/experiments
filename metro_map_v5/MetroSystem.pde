import java.util.Map;
import java.util.regex.Pattern;

class MetroSystem
{
  // grid size in pixels
  float grid_size = 50;
  float grid_w = floor(width / grid_size);
  float grid_h = floor(height / grid_size);

  // array of the metro lines
  ArrayList<MetroLine> lines = new ArrayList<MetroLine>();  
  // how many lines do we have...
  int num_lines;  
  int line_width = 15;
  // global metro line draw time
  float draw_time = 10000;

  // the font
  PFont font = createFont("DengXian Bold", 32);
  PFont names_font = createFont("DengXian Bold", 18);
  // separate graphics buffer for drawing the font names in the upper left hand corner
  PGraphics texter = createGraphics(width, height, P2D);

  // keeps track of the stations  
  HashMap<PVector, MetroStation> stations = new HashMap<PVector, MetroStation>(); 

  // spawning radius in grid blocks
  //float spawn_radius = 6.8;
  PVector spawn_radius = new PVector(width/(2 * grid_size) * 5/7, height/(2 * grid_size) * 3/7);

  // how bright is the background that we start drawing text using black instead of white
  float lum_thresh = 0.65 * 255;

  MetroSystem(int my_lines)
  {   

    num_lines = my_lines;
    MetroLine metroline;

    getStationNames();

    float base_hue = random(360);
    colorMode(HSB, 360, 100, 100);

    float start_angle, end_angle;

    for (int i = 0; i < num_lines; i++)
    {      
      start_angle = radians((base_hue + (1.*i)/(1.5*num_lines)*360.)%360.);
      end_angle = radians((base_hue + (2*i)/(3*num_lines)*360.)%360.);
      metroline = new MetroLine(this, start_angle, end_angle, i);

      // the lines' colors will be even spaced out hue-wise on the HSB color wheel
      metroline.c = color((base_hue + (1.*i)/(1.*num_lines)*360.)%360., random(50, 100), random(65, 100), 200);

      get_line_name(metroline);     

      metroline.line_width = this.line_width;
      lines.add(metroline);
    }
    // reset color mode for other drawing stuff...
    colorMode(RGB, 255, 255, 255, 255);
  }

  void update()
  {
    draw();
  }

  MetroLine tempy;
  MetroStation junct;
  PVector a_station = new PVector();
  void draw()
  {

    // draw lines
    for (int i = 0; i < num_lines; i++)
    {
      tempy = lines.get(i);
      tempy.update();
    }

    // draw stations
    for (Map.Entry me : stations.entrySet()) {      
      junct = (MetroStation)me.getValue();
      a_station = (PVector)me.getKey();
      junct.draw();
    }   

    // draw names
    for (int i = 0; i < num_lines; i++)
    {
      tempy = lines.get(i);
      rectMode(CORNER);
      stroke(20, 230);
      strokeWeight(1.5);
      fill(red(tempy.c), green(tempy.c), blue(tempy.c), 255);
      rect(50 - 5, 50+i*30 - 15, 
        textWidth(tempy.name) + 10, textAscent() * 1.5, 
        3, 3, 3, 3);      

      // see if text is too bright.. then choose black or white
      if (luminance(tempy.c) >= lum_thresh)
        fill(30);
      else
        fill(235);
      textFont(names_font);      
      text(tempy.name, 50, 50+i*30 + 1, 0);
    }

    // draw mouseover name
    drawName();
  }

  // for debugging
  void drawGridLines()
  {   
    stroke(204);
    strokeWeight(1);
    for (int i = 0; i < width/grid_size; i++)
    {
      line(i*grid_size, 0, i*grid_size, height);
      for (int j = 0; j < height/grid_size; j++)
      {
        line(0, j*grid_size, width, j*grid_size);
      }
    }
  }

  // give each line a name based on its color, grabbing color name from thecolorapi.com
  // - it returns the name of the closest named color!
  void get_line_name(MetroLine ml)
  {
    JSONObject data, sub_obj;
    data = loadJSONObject("http://www.thecolorapi.com/id?hex="+hex(ml.c, 6));
    sub_obj = data.getJSONObject("name");
    ml.name = sub_obj.getString("value") + " Line";
    println(ml.name);
  }

  // give each line a name based on its color, grabbing color name from colourlovers.com
  // - doesn't work for all colors >:( ==> CRASH
  String get_line_name_ALT(MetroLine ml)
  {
    println("getting name color " + hex(ml.c, 6));
    XML data = loadXML("http://www.colourlovers.com/api/color/"+hex(ml.c, 6));
    XML page = data.getChild("color").getChild("url");
    String s = page.getContent();

    String regex = "([a-zA-Z0-9_ ]+)";
    String[][] m = matchAll(s, regex);
    String _string = m[m.length-1][1];
    String[] parts = _string.split("_");
    String new_spaced = "";
    for (int i = 0; i < parts.length; i++)
    {
      new_spaced += parts[i] + " ";
    }
    return new_spaced.substring(0, new_spaced.length() - 1);
  }

  // list of possible forenames
  ArrayList<String> station_forenames = new ArrayList<String>();
  // list of possible surnames
  ArrayList<String> station_surnames = new ArrayList<String>();

  // runs the methods to get surnames & forenames
  void getStationNames()
  {
    getStationSurnames();
    getStationForenames();
  }

  // get station surnames from Wikipedia
  void getStationSurnames()
  {   
    // get the XML vesrion of the Wikipedia page!
    XML data = loadXML("https://en.wikipedia.org/wiki/Special:Export/List_of_building_types");
    // get the relevant text information from the page
    XML text = data.getChild("page").getChild("revision").getChild("text");

    // grab the text in the <text> tag
    String s = text.getContent();

    // use regular expressions to find words that come after a set of [['s    
    String regex = "(?:" + Pattern.quote("[") + Pattern.quote("[") + ")";
    regex += "([a-zA-Z0-9_" + Pattern.quote("-") + " ]*)";

    // add those words to the list of possible station surnames
    String[][] m = matchAll(s, regex);
    for (int i = 0; i < m.length-7; i++) {
      if (m[i][1].length() > 1)
      {
        station_surnames.add(capitalizeFirst(m[i][1]));
      }
    }
  }

  void getStationForenames()
  {
    String[] city_names = loadStrings("city_world.txt");
    for (int i = 0; i < city_names.length; i++) {
      //println(i);
      if (city_names[i].length() >= 1)
        station_forenames.add(capitalizeFirst(city_names[i]));
    }
  }

  // takes a bunch of words with spaces and capitalizes the first letters of each!
  String capitalizeFirst(String s)
  {
    String[] parts = s.split(" ");
    String capped = "";  
    for (int i = 0; i < parts.length; i++)
    {
      if (parts[i].equals("of"))
      {
        capped = "";
        continue;
      }

      if (needsCap(parts[i].charAt(0)))    
        capped += char(parts[i].charAt(0) - 32) + parts[i].substring(1, parts[i].length()) + " ";
      else
        capped += parts[i] + " ";
    }
    return capped.substring(0, capped.length() - 1);
  }

  // checks for lowercase letters
  boolean needsCap(char c)
  {
    return (int(c) >= 97 && int(c) <= 122);
  }

  // make a random name using the lists of names
  String getRandomName()
  {    
    // chooses which combination of forename/surname the station gets
    float rando = random(1);
    // forename only
    if (rando <= 0.2)
    {
      return station_forenames.get(round(random(station_forenames.size()-1)));
    }
    // surname only
    else if (rando > 0.2 && rando <= 0.35)
    {
      return station_surnames.get(round(random(station_surnames.size()-1)));
    }
    // both names
    else
    {
      return station_forenames.get(round(random(station_forenames.size()-1))) + 
        " " + 
        station_surnames.get(round(random(station_surnames.size()-1)));
    }
  }

  // return the nearest grid to the specified (x, y)
  PVector getNearestGrid(float x, float y)
  {
    PVector bobbo = new PVector();
    bobbo.x = (int)round(round(x / (grid_size))*grid_size);
    bobbo.y = (int)round(round(y / (grid_size))*grid_size);
    return bobbo;
  }

  // is this location within threshold of mouse
  boolean withinMouseDist(PVector p)
  {
    return sqrt(dist(p.x, p.y, mouseX, mouseY)) <= line_width * 1.8;
  }

  MetroStation temp_stat;
  PVector griddy;

  // draws the name of the mouse-hovered station!
  void drawName()
  {
    // black text
    fill(0);
    griddy = getNearestGrid(mouseX, mouseY);
    temp_stat = stations.get(griddy);
    // make sure it's near a grid
    if (withinMouseDist(griddy))
    {

      // make sure the grid has a MetroStation
      if (temp_stat != null)
      {
        float time_thing = map(sin(2*PI*millis()/700), -1, 1, 0.7, 1);
        float rad = line_width * 2.2 * time_thing;
        noFill();
        rectMode(CENTER);
        strokeWeight(1.5);        
        stroke(10, 200);
        ellipse(griddy.x, griddy.y, rad, rad);
        stroke(245, 100);
        ellipse(griddy.x, griddy.y, rad-2, rad-2);
        stroke(10, 200);
        ellipse(griddy.x, griddy.y, rad-8, rad-8);

        //text(temp_stat.name, width/2, height/10);
        int text_fill = 235;

        for (int i = 0; i < temp_stat.lines.size(); i++)
        {
          fill(red(temp_stat.lines.get(i).c), 
            green(temp_stat.lines.get(i).c), 
            blue(temp_stat.lines.get(i).c), 
            200);
          rectMode(CORNER);
          noStroke();
          // draw round rectangles...have to draw different rounded ones so it's one big
          //   rounded rectangles instead of multiple..
          if (i == 0 && i != temp_stat.lines.size() - 1)
          {
            rect(mouseX - (textWidth(temp_stat.name) + 10)/2, mouseY - (1.5)*textAscent() * (i+1)/temp_stat.lines.size()- (0.5)*1.5*textAscent(), 
              textWidth(temp_stat.name) + 10, 1.5*textAscent() * 1/temp_stat.lines.size(), 
              1, 1, 5, 5);
          } else if (i == temp_stat.lines.size() - 1)
          {
            rect(mouseX - (textWidth(temp_stat.name) + 10)/2, mouseY - (1.5)*textAscent() * (i+1)/temp_stat.lines.size()- (0.5)*1.5*textAscent(), 
              textWidth(temp_stat.name) + 10, 1.5*textAscent() * 1/temp_stat.lines.size(), 
              3, 3, 1, 1);
          } else
          {
            rect(mouseX - (textWidth(temp_stat.name) + 10)/2, mouseY - (1.5)*textAscent() * (i+1)/temp_stat.lines.size()- (0.5)*1.5*textAscent(), 
              textWidth(temp_stat.name) + 10, 1.5*textAscent() * 1/temp_stat.lines.size());
          }
          fill(0);
          if (luminance(temp_stat.lines.get(i).c) >= lum_thresh)
            text_fill = 30;
        }
        rectMode(CENTER);
        stroke(80);
        noFill();

        rect(mouseX, mouseY - 1.5*textAscent(), 
          textWidth(temp_stat.name) + 10, 1.5*textAscent(), 3, 3, 3, 3);

        fill(text_fill);
        text(temp_stat.name, mouseX - textWidth(temp_stat.name)/2, mouseY - 1.18*textAscent());
      }
      // debugging..
      else
      {
        //text(griddy + ", " + inBounds(griddy.x, griddy.y), width/2, height/10);
      }
    }
  }

  // check if it's in bounds of scren
  boolean inBounds(float x, float y)
  {
    return (x <= width && x >= 0 && y <= height && y >= 0);
  }

  float luminance(color c)
  {
    return  0.2126*red(c) + 0.7152*green(c) + 0.0722*blue(c);
  }

  float last_press = 0;
  void resetDrawTime()
  {
    if (last_press + draw_time <= millis())
    {
      last_press = millis();
      for (int i = 0; i < lines.size(); i++)
      {
        lines.get(i).resetDrawTime();
      }
    }
  }
}