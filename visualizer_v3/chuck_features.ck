// sound chain
SndBuf2 buffy => FFT fft =^ Centroid cent => blackhole;
buffy => dac;

fft =^ RMS rms => blackhole;
fft =^ RollOff roff50 => blackhole; 
fft =^ RollOff roff85 => blackhole;

// sound buf stuff
me.dir() + "nak - night sky.wav" => buffy.read;
//me.dir() + "Jake Kaufman - Mighty Switch Force 2 OST - 08 Tally Screen.wav" => buffy.read;
0 => buffy.pos;
1 => buffy.loop;

0.067 => buffy.rate;

// osc stuff
OscOut osc;
osc.dest("127.0.0.1", 60000);

// set parameters
512 => fft.size;

// set hann window
Windowing.hann(512) => fft.window;

// find our sampling rate
second / samp => float srate;

// set rolloff percentages
.5 => roff50.percent;
.85 => roff85.percent;

// main loop where we extract our features
while( true )
{
    //<<< " time to stuff " >>>;
    // takes our features
    cent.upchuck();
    rms.upchuck() @=> UAnaBlob blob;
    roff50.upchuck();
    roff85.upchuck();
    
    
    <<< "rms: ", Std.rmstodb(blob.fval(0)) >>>;
    <<< "centroid: ", cent.fval(0) * 360 >>>;
    <<< "rolloff 50: ",  roff50.fval(0) >>>;
    // osc sending
    oscOut("/rms", Std.rmstodb(blob.fval(0)));
    oscOut("/centroid", cent.fval(0) * 360);
    oscOut("/rolloff50", roff50.fval(0));
    oscOut("/rolloff50", roff85.fval(0));
    
    // advance time
    fft.size()::samp => now;
}

// osc sending function
fun void oscOut(string addr, float val) {
    osc.start(addr);
    osc.add(val);
    osc.send();
}