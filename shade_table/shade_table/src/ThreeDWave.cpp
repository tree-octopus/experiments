#include "ThreeDWave.h"
#include "ofApp.h";


ThreeDWave::ThreeDWave(float x_x, float y_y)
{
	this->x_off = x_x;
	this->y_off = y_y;
	excite();
}


ThreeDWave::~ThreeDWave()
{
}

void ThreeDWave::excite()
{
	start_time = ofGetElapsedTimef();
}

float ThreeDWave::getVal(float x, float y)
{
	noise_vel = fmod(ofGetElapsedTimef() - start_time, 1000);
	return ofMap(sin(sqrt(pow(x - x_off, 2) + pow(y - y_off, 2) + noise_vel_scale * noise_vel)), -1, 1, 0, 1);
}

bool ThreeDWave::isDead()
{
	noise_vel = fmod(ofGetElapsedTimef(), 1000);
	return exp(-exp_scale * (noise_vel_scale * noise_vel)) <= thresh;
}