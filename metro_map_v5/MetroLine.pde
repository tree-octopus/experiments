class MetroLine 
{
  // drawing stuff
  color c = #FF921A;
  float line_width = 15;

  // the name of the MetroLine
  String name = "";

  // minimum number of stations
  int min_stations = 3;
  // where each station is stored
  ArrayList<PVector> vertices = new ArrayList<PVector>();
  // list of this line's stations to prevent overlap!
  HashMap<PVector, Boolean> the_stations = new HashMap<PVector, Boolean>(); 


  // using this to determine the next segment's angle
  PVector[] rail_grower = {new PVector(-1, 1), new PVector(0, 1), new PVector(1, 1), new PVector(1, 0), 
    new PVector(1, -1), new PVector(0, -1), new PVector(-1, -1), new PVector(-1, 0)};

  // MetroSystem stuff
  // ========================================= //
  // which MetroSystem this is a part of
  MetroSystem ms;
  // which line this is in the MetroSystem!
  int line_number;
  float init_angle;
  float end_angle;
  int prev_track, temp_track;

  // has this line been constructed through the center?
  boolean is_seek_ctr = true;
  float seek_ctr = random(0.7, 1.0);
  // the center, where all good MetroLines should go
  PVector ctr = new PVector(width/2, height/2);  
  float seek_end = random(0.7, 1.0);
  // location of the destination 
  PVector end = new PVector();
  // have this line been constructed up to the destination point?
  boolean is_done = false;

  // drawing timing
  // ========================================= //
  // when to start drawing
  float start_time;
  // how long to take to draw each MetroLine
  float draw_time;

  MetroLine(MetroSystem _ms, float _angle, float _angle2, int _num)
  {
    ms = _ms;    
    line_number = _num;    
    draw_time = ms.draw_time;

    // choose random first point on the grid, in an ellipse radius
    // ========================================= //
    init_angle = _angle;
    end_angle = _angle2;

    float s_x = floor(width/(2 * ms.grid_size) + ms.spawn_radius.x*cos(init_angle))*ms.grid_size, 
      s_y = floor(height/(2 * ms.grid_size) + ms.spawn_radius.y*sin(init_angle))*ms.grid_size;
    float n_x, n_y;

    end.set(floor(width/(2 * ms.grid_size) + 0.8*ms.spawn_radius.x*cos(end_angle))*ms.grid_size, 
      floor(height/(2 * ms.grid_size) + 0.8*ms.spawn_radius.y*sin(end_angle))*ms.grid_size);    

    // first station
    // ========================================= //

    curr_pos = new PVector(s_x, s_y);
    vertices.add(curr_pos);
    trackStationsLocal(curr_pos);   
    temp_track = (int)random(rail_grower.length-1);
    trackStations(curr_pos);

    PVector curr_track_piece;

    curr_track_piece = rail_grower[temp_track];
    n_x =  curr_track_piece.x * ms.grid_size;  
    n_y =  curr_track_piece.y * ms.grid_size;

    while (!inBounds(n_x + s_x, n_y + s_y))
    {
      temp_track = (int)random(rail_grower.length-1);
      curr_track_piece = rail_grower[temp_track];
      n_x =  floor(curr_track_piece.x * ms.grid_size);  
      n_y =  floor(curr_track_piece.x * ms.grid_size);
    }
    s_x += n_x;
    s_y += n_y;
    prev_track = temp_track;
    curr_pos = new PVector(s_x, s_y);
    vertices.add(new PVector(s_x, s_y));
    trackStations(curr_pos);
    trackStationsLocal(curr_pos);
    // ========================================= //

    // other stations
    // ========================================= //
    //for (int i = 1; i < num_stations; i++)
    int num_iter = 0;
    while (is_seek_ctr && !its_over)
    {      
      //println("on: "+i+"th");      
      next_pos = chooseTrack();
      if (!its_over)
      {
        vertices.add(next_pos);
        prev_track = temp_track;
        curr_pos.set(next_pos);
        trackStations(curr_pos);
        trackStationsLocal(curr_pos);
        //println("adding vertex "+curr_pos);
      }

      num_iter++;

      if (getCurrDist(ctr) <= 2.5*ms.grid_size && 
        num_iter >= ms.grid_w * ms.grid_h / 2)
      {
        is_seek_ctr = false;
      }
    }
    its_over = false;
    num_iter = 0;
    while (!is_done && !its_over)
    {      
      //println("on: "+i+"th");      
      next_pos = chooseTrack();
      if (!its_over || vertices.size() < min_stations)
      {
        vertices.add(next_pos);
        prev_track = temp_track;
        curr_pos.set(next_pos);
        trackStations(curr_pos);
        trackStationsLocal(curr_pos);
        //println("adding vertex "+curr_pos);
      }

      num_iter ++;
      if (getCurrDist(end) <= 2.5*ms.grid_size && 
        num_iter >= ms.grid_w * ms.grid_h / 2)
      {
        is_done = true;
      }
    }

    //for (int i = 0; i < vertices.size(); i++)
    //{
    //  trackStations(vertices.get(i));
    //}
    // ========================================= //

    // set draw time relative to now!!
    start_time = millis();
    setEndPoints();
  }   

  float chooser;
  float curr_dist, next_dist;
  float thresh = 50;
  PVector next = new PVector();
  PVector curr_pos = new PVector();
  PVector next_pos = new PVector();
  boolean its_over = false;
  PVector chooseTrack()
  {
    // choose a random number in [0, 1]
    chooser = random(1);
    // if we're trying to go to the center...
    if (is_seek_ctr)
    {
      if (chooser <= seek_ctr)
      {
        //println("towards center");
        curr_dist = getCurrDist(ctr);
        int count = 0;
        do
        { 
          //println("A");          
          temp_track = chooseNext(prev_track);
          next = new PVector();
          next.set(rail_grower[temp_track]);
          //next.mult((int) random(3));
          //println(next);
          next.x *= ms.grid_size;
          next.y *= ms.grid_size;
          //println(next);
          next.x += curr_pos.x;
          next.y += curr_pos.y;
          //println("src : "+curr_pos);
          //println("dest: "+next);

          next_dist = dist(next.x, next.y, ctr.x, ctr.y);
          //println(curr_dist+" vs "+next_dist);
          //println("within: "+(abs(curr_dist - next_dist) < 1.5*ms.grid_size));
          //println("visited: "+visited(next.x, next.y));
          count++;
          if (count >= 10)
            its_over = true;
        }
        while (!its_over && !(inBounds(next.x, next.y) && (next_dist < curr_dist + 0*thresh && !visited(next.x, next.y))));     
        return next;
      } else
      {
        //println("random walk");
        do
        {
          //println("B");
          temp_track = chooseNext(prev_track);
          next = new PVector();
          next.set(rail_grower[temp_track]);
          next.x *= ms.grid_size;
          next.y *= ms.grid_size;
          next.add(curr_pos);
          //println(next);
        }
        while (!inBounds(next.x, next.y));
        return next;
      }
    }
    // if we're trying to go to the endpoint

    else if (!is_seek_ctr && !is_done)
    {
      if (chooser <= seek_end)
      {
        //println("towards end!!");
        curr_dist = getCurrDist(end);
        int count = 0;
        do
        { 
          //println("endA");          
          temp_track = chooseNext(prev_track);
          next = new PVector();
          next.set(rail_grower[temp_track]);
          //println(next);
          next.x *= ms.grid_size;
          next.y *= ms.grid_size;
          //println(next);
          next.x += curr_pos.x;
          next.y += curr_pos.y;
          //println("src : "+curr_pos);
          //println("dest: "+next);

          next_dist = dist(next.x, next.y, end.x, end.y);
          //println(curr_dist+" vs "+next_dist);
          //println("within: "+(abs(curr_dist - next_dist) < 1.5*ms.grid_size));
          //println("visited: "+visited(next.x, next.y));
          //println("threshed: "+(next_dist < curr_dist + thresh));
          count++;
          if (count >= 10)
            its_over = true;
        }
        while (!its_over && !(inBounds(next.x, next.y) && ((next_dist < curr_dist + 0.5*thresh) && !visited(next.x, next.y))));     
        return next;
      } else
      {
        //println("random walk to end");
        do
        {
          //println("endB");
          temp_track = chooseNext(prev_track);
          next = new PVector();
          next.set(rail_grower[temp_track]);
          next.x *= ms.grid_size;
          next.y *= ms.grid_size;
          next.add(curr_pos);
          //println(next);
        }
        while (!inBounds(next.x, next.y));
        return next;
      }
    } else
    {
      return new PVector();
    }
  }

  float getCurrDist(PVector p)
  {
    return dist(curr_pos.x, curr_pos.y, p.x, p.y);
  }

  boolean inBounds(float x, float y)
  {
    return (x <= width - ms.grid_size && x >= ms.grid_size && y <= height - ms.grid_size && y >= ms.grid_size);
  }

  boolean visited(float x, float y)
  {
    if (!inBounds(x, y))
    {
      return true;
    } else
    {
      return checkStationVisited(new PVector(x, y));
    }
  }

  boolean checkStationVisited(PVector vec)
  {
    return the_stations.get(vec) != null;
  }

  // this line's own list of MetroStations
  void trackStationsLocal(PVector vec)
  {
    if (the_stations.get(vec) == null)
    { 
      //println("putting "+vec);
      if (inBounds(vec.x, vec.y))
        the_stations.put(new PVector().set(vec), true);
    }
  }

  // keeping track of this line's stations, along with that of the other lines'
  //   by putting them all in the MetroSystem's hashmap
  void trackStations(PVector vec)
  {
    if (ms.stations.get(vec) == null && inBounds(vec.x, vec.y))
    {      
      ms.stations.put(new PVector().set(vec), new MetroStation(ms.getRandomName(), vec, this.ms));
      ((MetroStation)ms.stations.get(vec)).lines.add(this);
    } else if (inBounds(vec.x, vec.y))
    {
      if (!((MetroStation)ms.stations.get(vec)).lines.contains(this))
        ((MetroStation)ms.stations.get(vec)).lines.add(this);
      ((MetroStation)ms.stations.get(vec)).is_junct = true;
    }
  }

  void update()
  {
    draw();
  }

  PVector temp, temp_lerper = new PVector();  
  float i_limit, draw_period, prev_limit = 0;
  boolean reached = false;
  void draw()
  {
    i_limit = (vertices.size() * constrain((millis() - start_time)/(draw_time), 0., 1.));
    draw_period = draw_time/(vertices.size() - 1);
    beginShape();
    //reached = false;
    for (int i = 0; i < floor(i_limit); i++)
    {
      stroke(red(c), green(c), blue(c), 255);
      noFill();

      strokeWeight(line_width);
      strokeJoin(ROUND);
      strokeCap(ROUND);
      temp = vertices.get(i);
      if (i == floor(i_limit) - 1 && i > 0 && millis() <= start_time + draw_time)
      {
        temp_lerper.set(vertices.get(i-1));          
        float lerpy = (millis() % (draw_period))/(draw_period);
        lerpy *= lerpy;

        if (prev_limit != floor(i_limit) && abs(lerpy - 1) > 0.05)
        {
          reached = false;
        }

        if (abs(lerpy - 1) <= 0.05)
        {
          reached = true;
          lerpy = 1.0;
        }

        temp_lerper.lerp(temp, lerpy);

        if (!reached)
        {
          vertex(temp_lerper.x, temp_lerper.y);
        } else
        {
          vertex(temp.x, temp.y); 
          prev_limit = floor(i_limit);
        }
        //if(lerpy
      } else if (millis() > start_time)
      {          
        vertex(temp.x, temp.y);
      }         
      rectMode(CENTER);
    }
    endShape();
  }  

  void setEndPoints()
  {
    temp = vertices.get(0);
    ((MetroStation)ms.stations.get(temp)).is_end = true;
    ((MetroStation)ms.stations.get(temp)).line_end.add(line_number);
    temp = vertices.get(vertices.size() - 1);
    ((MetroStation)ms.stations.get(temp)).is_end = true;
    ((MetroStation)ms.stations.get(temp)).line_end.add(line_number);
  }

  // start drawing again!!
  void resetDrawTime()
  {
    start_time = millis();
  }

  // chooses the next segment, based on rules...avoids sharp +/- 135 deg angles!! 
  int chooseNext(int a)
  {
    int billy = a + -2 + round(random(4));
    billy += rail_grower.length;
    billy %= rail_grower.length;
    return billy;
  }
}