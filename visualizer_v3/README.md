# Visualizer V3
*Processing, Shaders, ChucK*

<img src="https://i.imgur.com/miXZ6ZN.gif" alt="" width="40%" height="40%">

Uses shaders to make a night sky music visualizer. 

Clouds are a Fractal Brownian Motion (fbm) shader, and the stars are a Voronoi Diagram shader drawing the cell centers.

Music reactivity is added in ChucK, calculating RMS and sending it via OSC;