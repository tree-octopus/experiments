#pragma once

#include "ofMain.h"
#include "ThreeDWave.h"

class ofApp : public ofBaseApp {
public:

	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h); 
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	void audioIn(float * input, int buffer_size, int n_channels);
	void audioOut(float * output, int bufferSize, int nChannels);

	// audio stuff
	int initialBufferSize;
	int sampleRate;

	float freq = 140.;
	float freq_step = 10;
	float ph = 0.;
	float ampl = 0.5;
	float ampl_step = 0.02;
	float time_scale = 1.;
	float time_scale_step = 0.1;

	int curr_wave = 0;
	vector<string> waves = { "shadersGL3/manhattan_voronoi_YB_v1.frag",
		"shadersGl3/basic_quad.frag",
		"shadersGL3/3d_cell_noise.frag",
		"shadersGL3/pixelized_radius_thing.frag",
		"shadersGL3/downsampled_noise.frag",
		"shadersGL3/fbm_circle.frag" ,
		"shadersGL3/bubble_voronoi.frag",
		"shadersGL3/swirly.frag",
		"shadersGL3/voronoi_clumped_kaleidoscope.frag"
	};

	// which part of the image are we sampling;
	float wave_grabbed = 0;

	// how large to draw previewed image
	float preview_scale = 1 / 5.;

	ofMesh meshy;
	// since audio thread runs separately from visuals,
	//	we need to make sure visuals/wave table is ready
	//  before we can have audio!
	bool is_ready = false;


	// graphics stuff
	ofShader disp_shader;
	ofPlanePrimitive plane;
	int rows = 60, cols = 80;
	ofImage img;

	// audio output generated from wave table
	vector<float> audio_out;	
	vector<float> wave_table;

	float scale_thing = 1.;
	ofColor color;	
	ofShader frag;
	ofFbo fbo;

};
