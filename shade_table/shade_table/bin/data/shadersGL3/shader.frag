#version 150

uniform sampler2DRect tex0;
uniform vec2 u_resolution;
in vec2 texCoordVarying;

out vec4 outputColor;

in vec4 v_color;
uniform vec4 base_color;

void main()
{	
	float r = gl_FragCoord.x / u_resolution.x;
    float g = gl_FragCoord.y / u_resolution.y;
    float b = 1.0;
    float a = 1.0;    
    outputColor = mix(texture(tex0, texCoordVarying),  v_color, 0.5);
    outputColor *= vec4(r,g,b,a);
}