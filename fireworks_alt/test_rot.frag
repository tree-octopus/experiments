// Author @patriciogv - 2015
// http://patriciogonzalezvivo.com

#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

float manhattan(vec2 a, vec2 b)
{
    return pow(abs(a.x - b.x) + abs(a.y - b.y), 0.512);    
}

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}

void main(){
	vec2 st = gl_FragCoord.xy/u_resolution;
    float pct = 0.0;

    
    vec2 st_rot = st;
    st_rot -= vec2(0.5);
    st_rot *= rotate2d(3.404 * u_time * 2./4.);
    st_rot += vec2(0.5);
    // a. The DISTANCE from the pixel to the center
    pct = 1.- pow(manhattan(st_rot,vec2(0.5)), 0.20*sin(u_time*5.)+.5);


    pct *= 1. - pow(distance(st, vec2(0.5)), 2.552);
    
    vec3 color = vec3(pow(pct, 2.0));

	gl_FragColor = vec4( color, 1.0 );
}
