// fireworks thingy
// ===============================================
// author: jeff huang, calarts
// uses distance shader to draw gradients for fireworks
//
// https://gitlab.com/tree-octopus/experiments/tree/master/fireworks_alt
// 

// spring to control drawing of the fireworks
Spring sp;

// lissajous coefficients
float a = width * 1 + 0.3, b = height * 3 + 0.5;

ParticleSystem ps;

void setup() {
  //fullScreen(P2D);
  size(1600, 900, P3D);
  //size(900, 900, P3D);

  ps = new ParticleSystem(false, false);
  sp = new Spring(0.24, 0.005, 0.87, new PVector(random(width), random(height)), new PVector(random(width), random(height)));

  // graphics settings
  noStroke();
  smooth();    
  blendMode(BLEND);
}

// fractal brownian motion
// - adding together several octaves of noise to get a more dynamic, self-similar feel
int octaves = 4;

float fbm (float x, float y)
{
  float val = 0;
  float amp = 0.5;
  for (int i = 0; i < octaves; i ++)
  {
    val += amp * noise(x, y);
    x *= 2;
    y *= 2;
    amp *= 0.5;
  }
  return val;
}

void draw() {  
  rectMode(CORNER);

  // don't watn to draw the background with the shader,
  //    want to draw it normally!
  blendMode(BLEND);
  // don't draw the background using the particle shader...
  resetShader();  
  noStroke();    

  // modulate transparency of background with sin
  // - lower values will make longer trails, 
  //      but will also leave blending artifacts 
  //      in the background
  // bug: sometimes randomly stops drawing the particles...
  fill(0, constrain(map(sin(second() + noise(second())), -1, 1, 13, 25), 13, 25));
  //fill(0, 21);
  rect(0, 0, width, height);


  // modulate lissajous coefficients by noise
  a = map(fbm(sp.pos.x + second(), sp.pos.y + second()), 0, 1, 0, .8) * width;
  b = map(fbm(sp.pos.x + second(), sp.pos.y + second()), 0, 1, 0, .8) * height;

  // move the rest position, but randomly in time too!
  // - makes it pause in some locations for a short time
  if (frameCount % int(random(1, 30)) == 0)
  {
    // - move the rest position of the spring, so the spring moves semi-chaotically
    // - modulate lissajous center position by noise as well
    sp.rest = new PVector(
      a * cos(0.0008 * millis())+ width/2 * (1. + 0.3* (2 * noise(second()) - 1)), 
      b * sin(0.0009 * millis())+ height/2 * (1. + 0.4 * (2 * noise(second()) - 1))
      );
  } else
  {
  }

  sp.update();


  // for debugging purposes: draw where the spring is
  // -----
  //resetShader();
  //fill(0, 255, 0);
  //ellipse(sp.pos.x, sp.pos.y, 5, 5);


  // keep particles under control, only try to spawn new particles
  //   every other frame
  if (frameCount % 2 == 0)
  {
    ps.explode((int)sp.pos.x, (int)sp.pos.y);
  }

  ps.update();  

  // color shift every 8 or so seconds
  if (second() % 8 < 0.01)
  {
    ps.colorShift_threaded();
  }
    
}

// can try to drag fireworks!
void mouseDragged()
{
  ps.explode(mouseX, mouseY);
}

// press shift to change colors
void keyPressed()
{  
  if (key == CODED) {
    if (keyCode == SHIFT)
    {
      ps.colorShift_threaded();
    }
  }
}

// color changing thread that runs independently
// - has to be in the main sketch file!
void color_thread()
{
  if (ps.is_threading)
  {
    println("i'm threading");
    while (millis() < ps.start_time + ps.lerp_time)
    {
      ps.colors[0] = lerpColor(ps.src1, ps.dest1, (millis() - ps.start_time)/ps.lerp_time);
      ps.colors[1] = lerpColor(ps.src2, ps.dest2, (millis() - ps.start_time)/ps.lerp_time);
    }
    ps.is_threading = false;
    println("done!");
  }
}