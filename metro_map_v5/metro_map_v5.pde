MetroSystem ms;
float cooldown = 4000;
float start_time;

PShader vignette;
PShader voronoi_riv;
PShader voronoi_st;

// frame buffer for combining images & stuff
PGraphics pg;
void setup()
{
  // graphics stuff
  // ========================================= /
  size(1600, 800, P2D);
  //size(700, 500, P2D);
  //fullScreen(P2D);
  pg = createGraphics(width, height, P3D);
  pg.smooth();

  vignette = loadShader("vignetting.frag"); 
  voronoi_riv = loadShader("bw_manhattan_voronoi_riv.frag");
  voronoi_riv.set("seed", 1. + random(100));
  voronoi_st = loadShader("bw_manhattan_voronoi.frag");
  voronoi_st.set("seed", 1. + random(100));
  smooth(8);
  frameRate(1000);
  // ========================================= /

  // create a MetroSystem with a random number of lines
  ms = new MetroSystem((int)random(3, 7));  

  // timing
  start_time = millis();
}


void draw()
{
  background(245);
  blendMode(BLEND);

  // river
  // ========================================= /
  pg.beginDraw();
  pg.resetShader();  

  pg.shader(voronoi_riv);
  pg.filter(voronoi_riv);
  voronoi_riv.set("u_resolution", float(width), float(height));
  voronoi_riv.set("u_time", millis() / 1000.0);
  voronoi_riv.set("u_mouse", (float)mouseX, (height - mouseY) * 1.0);
  voronoi_riv.set("color", (float)245/255, (float)245/255, (float)245/255);

  pg.rectMode(CORNER);
  pg.rect(0, 0, width, height);
  pg.endDraw();

  noStroke();
  image(pg, 0, 0);
  // ========================================= /

  // streets
  // ========================================= /
  pg.beginDraw();
  pg.resetShader();  

  pg.shader(voronoi_st);
  pg.filter(voronoi_st);
  voronoi_st.set("u_resolution", float(width), float(height));
  voronoi_st.set("u_time", millis() / 1000.0);
  voronoi_st.set("u_mouse", (float)mouseX, (height - mouseY) * 1.0);
  voronoi_st.set("color", (float)245/255, (float)245/255, (float)245/255);
  pg.rectMode(CORNER);
  pg.rect(0, 0, width, height);
  pg.endDraw();

  image(pg, 0, 0);
  // ========================================= /

  // vignetting 
  // ========================================= /  
  pg.beginDraw();
  pg.resetShader();  

  pg.shader(vignette);
  pg.filter(vignette);
  vignette.set("u_resolution", float(width), float(height));
  vignette.set("u_time", millis() / 1000.0);
  vignette.set("u_mouse", (float)mouseX, (height - mouseY) * 1.0);
  vignette.set("color", (float)245/255, (float)245/255, (float)245/255);
  pg.rectMode(CORNER);
  pg.rect(0, 0, width, height);
  pg.endDraw();
  blendMode(MULTIPLY);
  image(pg, 0, 0);
  // ========================================= /
  blendMode(BLEND);
  // update MetroSystem
  // ========================================= /
  ms.update();
  rectMode(CORNER);
  // ========================================= /
  //fill(0);
}

void keyPressed()
{
  // check for reset key
  if (key == CODED) {
    // only reset if it's off of cooldown!!
    if (keyCode == RIGHT && millis() >= start_time + cooldown)
    {
      // generate a new metro system!
      ms = new MetroSystem((int)random(3, 7));
      start_time = millis();
      voronoi_riv.set("seed", 1. + random(100));
      voronoi_st.set("seed", random(100));
    }
    if(keyCode == LEFT)
    {
      ms.resetDrawTime();
    }
  }
}