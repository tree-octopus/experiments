ParticleSystem ps;
LeafSystem ls;

float cc;
color bg = color(204, 204, 204);
void setup()
{
  size(1600, 900, P3D);
  //fullScreen(P3D, 2);
  
  
  ls = new LeafSystem();
  ps = new ParticleSystem(false, false, ls);
  
  cc = hue(get(0,0));
  //noSmooth();  
  //color c = #014570;
  //background(c);
}

void draw()
{
  if(drawBG)
  {  
    colorMode(HSB, 360, 255, 255);
    //color c = #014570;
    color c = ps.colors[3];
    // choose a random background color that is analogous/complementary
    cc = hue(c) + 360./((int)random(10)+1);
    cc %= 360;
    //println(cc);
    c = color(cc, saturation(c)/random(1,4), map(brightness(c), 0, 255, 0, 255));
    //hue((int)map(abs(cc), 0, 359, 0, 255)%256);
    
    //brightness(250);

    fill(c);
    
    // draw rectangle over entire screen to erase past frame history
    rectMode(CORNER);
    rect(0, 0, width, height);
    drawBG = false; 
    colorMode(RGB, 255, 255, 255);
  }
  
  
  //ls.update();
  ps.update();
  //blendMode(DIFFERENCE);
}

void mouseMoved()
{
 //bob.setPos(mouseX, mouseY); 
}

void mousePressed()
{
  // can only spawn tree once old tree is done growing (to prevent slowdown)
  if (mouseButton == LEFT && ps.coolDown())
  {
     ps.explode(mouseX, mouseY); 
  }
  // make the leaves fall
  if (mouseButton == RIGHT)
  {
     ls.leafFall(); 
  }
}

boolean drawBG = false;


void keyPressed()
{
  // check for reset key
   if (key == CODED) {
     if(keyCode == SHIFT)
     {
       drawBG = true;
       //println("key");
    }
   }
}