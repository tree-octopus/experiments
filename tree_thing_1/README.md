# tree spawner
*Processing*

<img src="https://i.imgur.com/nFKnluw.gif" alt="" width="540" height="300">

Makes trees using a particle system. Random walks are used to add noise to the distribution of the branches and leaves.